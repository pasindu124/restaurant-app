package com.app.restaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class RestaurantApplication {
    @Autowired
	@GetMapping("/home")
	public String home() {
		return "Hello Docker World";
	}

	@PostMapping("/login")
	public boolean login() {
		return true;
	}

//	@PostMapping("/user")
//	public User createUser(@Valid @RequestBody User user) {
//        System.out.println(user.getRoles());
//	    return userRepository.save(user);
//	}


	public static void main(String[] args) {
		SpringApplication.run(RestaurantApplication.class, args);
	}

}
