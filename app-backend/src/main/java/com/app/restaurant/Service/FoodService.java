package com.app.restaurant.Service;

import com.app.restaurant.Model.DAOUser;
import com.app.restaurant.Repository.FoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.app.restaurant.Model.Food;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class FoodService {

    @Autowired
    FoodRepository repository;

    public Food addFood(Food entity) throws Exception
    {
        entity = repository.save(entity);

        return entity;
    }

    public void deleteFood(Integer id) throws Exception
    {
        Optional<Food> entity = repository.findById(id);
        if(entity.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new Exception("No food record exist for given id");
        }
    }

    public Food getFood(Integer id) throws Exception
    {
        Optional<Food> entity = repository.findById(id);

        if(entity.isPresent()) {
            return entity.get();
        } else {
            throw new Exception("No food record exist for given id");
        }
    }

    public List<Food> getAllFood(DAOUser user) throws Exception
    {
        List<Food> entity = repository.findByDaoUser(user);
        return entity;
    }

    public Food updateFood(Integer id, Map<String, Object> payload) throws Exception
    {
        Food entity = repository.findById(id).get();
        entity.setDescription(payload.get("description").toString());
        entity.setName(payload.get("name").toString());
        entity.setPrice(payload.get("price").toString());

        return repository.save(entity);
    }
}
