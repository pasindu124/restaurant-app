package com.app.restaurant.Service;

import com.app.restaurant.Model.*;
import com.app.restaurant.Repository.FoodRepository;
import com.app.restaurant.Repository.OrderRepository;
import com.app.restaurant.Repository.UserDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class Consumer {


    @Autowired
    Producer producer;

    @Autowired
    OrderRepository orderRepo;

    @Autowired
    FoodRepository foodRepo;

    @Autowired
    UserDao userDao;

    @KafkaListener(topics = "restaurants", groupId = "group_id")
    public void consume(String message) throws IOException {
        producer.sendRestaurantList();
    }

    @KafkaListener(topics = "food_order", groupId = "group_id")
    public void consumeFoodOrder(String order) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        FoodOrderModel foodorder = mapper.readValue(order, FoodOrderModel.class);
        DAOUser user = userDao.findByUsername(foodorder.getRes_username());
        Orders newOrder = new Orders();
        newOrder.setCusname(foodorder.getCus_name());
        newOrder.setCustel(foodorder.getCus_tel());
        newOrder.setDaoUser(user);
        newOrder.setOrder_status("pending");
        newOrder.setDelivery_status("pending");
//        Iterable<Integer> foodIds = Arrays.asList(foodorder.getFood_list());
//        Food food = foodRepo.findById(2).get();
//        food.getOrders().add(newOrder);
//        newOrder.getFoods().add(food);

        //       Set<Food> set = new HashSet(foods);
//        newOrder.setFoods(set);

        orderRepo.save(newOrder);
    }

    @KafkaListener(topics = "rider_accept", groupId = "group_id")
    public void consumeRiderAccept(String rider) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        riderAcceptReq newrider = mapper.readValue(rider, riderAcceptReq.class);
        Orders order = orderRepo.findById(newrider.getOrderId()).get();
        order.setDelivery_status("ongoing");
        order.setRidername(newrider.getRiderName());
        order.setRidertel(newrider.getRiderTelNo());

        orderRepo.save(order);
    }

    @KafkaListener(topics = "rider_delivered", groupId = "group_id")
    public void consumeRiderDelivered(String orderid) throws IOException {
        Orders order = orderRepo.findById(Integer.parseInt(orderid)).get();
        order.setDelivery_status("delivered");

        orderRepo.save(order);
    }
}