package com.app.restaurant.Service;

import com.app.restaurant.Model.DAOUser;
import com.app.restaurant.Model.FoodOrderModel;
import com.app.restaurant.Model.riderAcceptReq;
import com.app.restaurant.Repository.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);
    private static final String TOPIC = "test";

    @Autowired
    private KafkaTemplate<String, List<DAOUser>> kafkaTemplate;

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplateString;

    @Autowired
    private KafkaTemplate<String, FoodOrderModel> kafkaTemplateFoodOrder;

    @Autowired
    private KafkaTemplate<String, riderAcceptReq> kafkaTemplateRiderAccept;

    @Autowired
    private UserDao userDao;

    public void sendMessage() {
        List<DAOUser> entity = (List<DAOUser>) userDao.findAll();
        this.kafkaTemplate.send(TOPIC, entity);
    }

    public void sendRestaurantList() {
        List<DAOUser> entity = (List<DAOUser>) userDao.findAll();
        this.kafkaTemplate.send("restaurants_list", entity);
    }

    public void requestRider(String orderid) {
        this.kafkaTemplateString.send("available_riders_req", orderid);
    }

//    public void getAvailableRiders() {
//        this.kafkaTemplateString.send("available_riders_req", "");
//    }

    public void sendTestFoodOrder() {
        FoodOrderModel foodOrder = new FoodOrderModel("pasindu", new Integer[] {1, 2}, "krishan", "09231");
        this.kafkaTemplateFoodOrder.send("food_order", foodOrder);
    }

    public void sendTestRiderAccept() {
        riderAcceptReq rider = new riderAcceptReq(18,"Kasun Lakshitha", "0712727787");
        this.kafkaTemplateRiderAccept.send("rider_accept", rider);
    }

    public void sendTestRiderDelivery(){
        this.kafkaTemplateString.send("rider_delivered", "18");
    }

}
