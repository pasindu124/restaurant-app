package com.app.restaurant.Service;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.app.restaurant.Repository.UserDao;
import com.app.restaurant.Model.DAOUser;
import com.app.restaurant.Model.UserDTO;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        DAOUser user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>());
    }

    public DAOUser save(UserDTO user) {
        DAOUser newUser = new DAOUser();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setName(user.getName());
        newUser.setTelno(user.getTelno());
        newUser.setAddress(user.getTelno());
        newUser.setCity(user.getCity());
        return userDao.save(newUser);
    }

    public DAOUser getUser(String username) {
        DAOUser user = userDao.findByUsername(username);
        return user;
    }

    public DAOUser updateUser(Map<String, Object> payload) {
        final String username = payload.get("username").toString();
        DAOUser user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        user.setCity(payload.get("city").toString());
        user.setName(payload.get("name").toString());

        user.setTelno(payload.get("telno").toString());
        user.setAddress(payload.get("address").toString());

        return userDao.save(user);
    }
}
