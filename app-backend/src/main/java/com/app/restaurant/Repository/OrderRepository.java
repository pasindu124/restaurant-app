package com.app.restaurant.Repository;

import com.app.restaurant.Model.DAOUser;
import com.app.restaurant.Model.Food;
import com.app.restaurant.Model.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Orders,Integer>{
    List<Orders> findByDaoUser(DAOUser user);
}
