package com.app.restaurant.Repository;

import com.app.restaurant.Model.DAOUser;
import com.app.restaurant.Model.Food;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FoodRepository extends JpaRepository<Food,Integer>{
    List<Food> findByDaoUser(DAOUser user);
}
