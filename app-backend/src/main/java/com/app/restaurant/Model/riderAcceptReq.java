package com.app.restaurant.Model;

public class riderAcceptReq {

    private int orderId;
    private String riderName;
    private String riderTelNo;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getRiderName() {
        return riderName;
    }

    public void setRiderName(String riderName) {
        this.riderName = riderName;
    }

    public String getRiderTelNo() {
        return riderTelNo;
    }

    public void setRiderTelNo(String riderTelNo) {
        this.riderTelNo = riderTelNo;
    }

    public riderAcceptReq() {
    }

    public riderAcceptReq(int orderId, String riderName, String riderTelNo) {
        this.orderId = orderId;
        this.riderName = riderName;
        this.riderTelNo = riderTelNo;
    }
}
