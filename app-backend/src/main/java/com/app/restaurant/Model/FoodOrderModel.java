package com.app.restaurant.Model;

public class FoodOrderModel {
    private String res_username;

    private Integer[] food_list;

    private String cus_name;

    private String cus_tel;

    public String getRes_username() {
        return res_username;
    }

    public void setRes_username(String res_username) {
        this.res_username = res_username;
    }

    public Integer[] getFood_list() {
        return food_list;
    }

    public void setFood_list(Integer[] food_list) {
        this.food_list = food_list;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_tel() {
        return cus_tel;
    }

    public void setCus_tel(String cus_tel) {
        this.cus_tel = cus_tel;
    }

    public FoodOrderModel() {

    }

    public FoodOrderModel(String res_username, Integer[] food_list, String cus_name, String cus_tel) {
        this.res_username = res_username;
        this.food_list = food_list;
        this.cus_name = cus_name;
        this.cus_tel = cus_tel;
    }
}
