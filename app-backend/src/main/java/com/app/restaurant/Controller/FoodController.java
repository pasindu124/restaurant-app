package com.app.restaurant.Controller;


import com.app.restaurant.Model.DAOUser;
import com.app.restaurant.Model.Food;
import com.app.restaurant.Repository.UserDao;
import com.app.restaurant.Service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/fooditem")
public class FoodController {

    @Autowired
    FoodService foodService;

    @Autowired
    private UserDao userDao;

    @PostMapping
    public ResponseEntity<?> addFood(@RequestBody Map<String, Object> payload) throws Exception {
        String name = payload.get("name").toString();
        String price = payload.get("price").toString();
        String description = payload.get("description").toString();
        DAOUser user = userDao.findByUsername(payload.get("username").toString());
        Food food = new Food(name, price, description, user);
        return ResponseEntity.ok(foodService.addFood(food));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getFood(@PathVariable("id") Integer id) throws Exception {
        return ResponseEntity.ok(foodService.getFood(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateFood(@PathVariable("id") Integer id, @RequestBody Map<String, Object> payload) throws Exception {
        return ResponseEntity.ok(foodService.updateFood(id, payload));
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteFood(@PathVariable("id") Integer id) throws Exception {
        foodService.deleteFood(id);
        return HttpStatus.FORBIDDEN;
    }

    @GetMapping
    public ResponseEntity<?> getAllFood(@RequestParam("username") String username) throws Exception {
        DAOUser user = userDao.findByUsername(username);
        return ResponseEntity.ok(foodService.getAllFood(user));
    }
}
