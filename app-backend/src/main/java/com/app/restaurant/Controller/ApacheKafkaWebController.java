package com.app.restaurant.Controller;

import com.app.restaurant.Model.Orders;
import com.app.restaurant.Repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.app.restaurant.Service.KafkaSender;

import com.app.restaurant.Service.Producer;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/kafka-server/")
public class ApacheKafkaWebController {

    @Autowired
    Producer producer;

    @Autowired
    KafkaSender kafkaSender;

    @Autowired
    OrderRepository orderRepo;

    @GetMapping(value = "/producer")
    public String producer(@RequestParam("message") String message) {
        kafkaSender.send(message);

        return "Message sent to the Kafka Topic Successfully";
    }

    @PostMapping(value = "/publish")
    public void sendMessageToKafkaTopic(@RequestParam("message") String message) {
        this.producer.sendMessage();
    }

    @PostMapping(value = "/sendtestfoodorder")
    public void sendFoodOrderToKafkaTopic() {
        this.producer.sendTestFoodOrder();
    }
    @PostMapping(value = "/requestrider")
    public ResponseEntity<?> requestRider(@RequestBody Map<String, Object> payload) throws Exception {
        Orders order =  orderRepo.findById(Integer.parseInt(payload.get("id").toString())).get();
        order.setDelivery_status("requested");
        orderRepo.save(order);
        this.producer.requestRider(payload.get("id").toString());
        return ResponseEntity.ok(orderRepo.save(order));
    }

    @PostMapping(value = "/sendtestrideraccept")
    public void sendTestRiderAcceptToKafkaTopic() {
        this.producer.sendTestRiderAccept();
    }

    @PostMapping(value = "/sendtestriderdelivery")
    public void sendTestRiderDeliveryToKafkaTopic() {
        this.producer.sendTestRiderDelivery();
    }

}
