package com.app.restaurant.Controller;


import com.app.restaurant.Model.DAOUser;
import com.app.restaurant.Model.Food;
import com.app.restaurant.Model.Orders;
import com.app.restaurant.Repository.OrderRepository;
import com.app.restaurant.Repository.UserDao;
import com.app.restaurant.Service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value = "/orders")
public class OrderController {

    @Autowired
    OrderRepository repository;

    @Autowired
    private UserDao userDao;

    @GetMapping
    public ResponseEntity<?> getAllOrders(@RequestParam("username") String username) throws Exception {
        DAOUser user = userDao.findByUsername(username);
        List<Orders> entities = repository.findByDaoUser(user);
        return ResponseEntity.ok(entities);
    }

    @PostMapping(value = "/accept")
    public ResponseEntity<?> acceptOrder(@RequestBody Map<String, Object> payload) throws Exception {
        int id = Integer.parseInt(payload.get("id").toString());

        Orders order = repository.findById(id).get();
        order.setOrder_status("accepted");

        return ResponseEntity.ok(repository.save(order));

    }

    @PostMapping(value = "/reject")
    public ResponseEntity<?> rejectOrder(@RequestBody Map<String, Object> payload) throws Exception {
        int id = Integer.parseInt(payload.get("id").toString());

        Orders order = repository.findById(id).get();
        order.setOrder_status("rejected");

        return ResponseEntity.ok(repository.save(order));

    }
    
}
