

run mysql

docker run -d -p 3306:3306 --name=docker-mysql --env="MYSQL_ROOT_PASSWORD=password" --env="MYSQL_PASSWORD=password" --env="MYSQL_DATABASE=restaurant" mysql:8

./gradlew build
docker build -f Dockerfile -t restuarant-app .
docker run --network="host" -p 3306:3306 -p 8080:8080 restuarant-app