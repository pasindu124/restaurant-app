
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { Table, Divider, Tag, Button } from 'antd';
import clickApi from '../../../src/services/clickapi';
import router from 'umi/router';
let data = [];

class FoodTable extends React.Component {
    state = {
        tableData: []
    };
    constructor(props) {
        super(props);
        this.clickApi = new clickApi();
        this.onDelete = this.onDelete.bind(this);
        this.onEdit = this.onEdit.bind(this);

    }

    onDelete(record, e) {
        e.preventDefault();

        return this.clickApi.deleteData(`fooditem/${record.key}`).then((res) => {
            let index = data.indexOf(record);
            if (index > -1) {
                data.splice(index, 1);
                this.setState({tableData:data})
            }
        })
    }

    onEdit(record, e) {
      e.preventDefault();
      router.push(`/foods/list/${record.key}`)
    }

    componentDidMount() {
        this.clickApi.getData(`fooditem?username=${this.props.currentUser.username}`).then(response => {
            data = []
           response.map((food) => {
                let item = {
                    key: food.id,
                    name: food.name,
                    price: food.price,
                    description: food.description
                }
                data.push(item)
           })
           this.setState({tableData:data})
        });
    }

    render() {
        const columns = [
            {
              title: 'Name',
              dataIndex: 'name',
              key: 'name',
              render: text => <a>{text}</a>,
            },
            {
              title: 'Price (Rs.)',
              dataIndex: 'price',
              key: 'price',
            },
            {
              title: 'Description',
              dataIndex: 'description',
              key: 'description',
            },
            {
              title: 'Action',
              key: 'action',
              render: (text, record) => (
                <span>
                  <a onClick={(e) => this.onEdit(record, e)}>Edit</a>
                  <Divider type="vertical" />
                  <Button onClick={(e) => this.onDelete(record, e)} type="danger">Delete</Button>
                </span>
              ),
            },
        ];
        return (
            <Table columns={columns} dataSource={this.state.tableData} />
        );
    }
}

export default FoodTable;          