
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import clickApi from '../../../src/services/clickapi';

import {
  Form,
  Input,
  Tooltip,
  Icon,
  Cascader,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  AutoComplete,
} from 'antd';
const { TextArea } = Input;
import router from 'umi/router';



class RegistrationForm extends React.Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
  };

  constructor(props) {
    super(props);
    this.clickApi = new clickApi();
    this.currentFood = {}
    
    }

  componentDidMount() {
     
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {

        this.props.callBackSubmit(values)
       //this.props.callbackUpdateUser(values);
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  handleWebsiteChange = value => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
    }
    this.setState({ autoCompleteResult });
  };

  render() {
    const { getFieldDecorator } = this.props.form;  

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    

    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        
        <Form.Item
          label="Name"
        >
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please input your food name!', whitespace: true }],
            initialValue: this.props.currentFood.name
          })(<Input />)}
        </Form.Item>
        <Form.Item
          label="Price"
        >
          {getFieldDecorator('price', {
            rules: [{ required: true, message: 'Please input your food price!', whitespace: true }],
            initialValue: this.props.currentFood.price
         })(<Input />)}
        </Form.Item>
        <Form.Item
          label="Description"
        >
          {getFieldDecorator('description', {
            rules: [{ required: true, message: 'Please input your food description!', whitespace: true }],
            initialValue: this.props.currentFood.description
         })(<TextArea />)}
        </Form.Item>
        
        <Form.Item {...tailFormItemLayout}>
          
          {
              this.props.currentFood.name ? 
              <Button type="primary" htmlType="submit">
              Update
            </Button> : <Button type="primary" htmlType="submit">
              Add
            </Button>
          }
        </Form.Item>
      </Form>
    );
  }
}

const FoodAddForm = Form.create({ name: 'update' })(RegistrationForm);
export default FoodAddForm;
          