import React from 'react';
import { Card, Typography, Alert, Icon } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import 'antd/dist/antd.css';
import router from 'umi/router';
import FoodAddForm from './food-add-form';
import clickApi from '../../../src/services/clickapi';


class FoodView extends React.Component {
    state = {
        currentFood: {}
    }
    constructor(props) {
        super(props);
        const token =  localStorage.getItem('restaurant-app-token');
        this.clickApi = new clickApi();
        if (!token) {
            router.push(`/user/login`);
        }
        this.handleEditFood = this.handleEditFood.bind(this);
        

    }
    componentDidMount() {
        this.clickApi.getData(`fooditem/${this.props.match.params.id}`).then((res) => {
            if(!res.error) {
                this.currentFood = res;
                this.setState({currentFood:res})
            } else {
                router.push(`/home`);
            }

        });
    }


    handleEditFood(values) {
        return this.clickApi.putData(values, `fooditem/${this.props.match.params.id}`).then((res) => {
            router.push(`/foods/list`);
        });
    }

    render() {
        const currentUser =  JSON.parse(localStorage.getItem('current-user'));
        //this.currentFood = {ab:"pa"}
       

        return (
            <PageHeaderWrapper content="Foods Add">
            <Card>
                <FoodAddForm callBackSubmit={this.handleEditFood} currentFood={this.state.currentFood}/>
            </Card>
          
        </PageHeaderWrapper>
        );
    }
}


export default FoodView;
          
