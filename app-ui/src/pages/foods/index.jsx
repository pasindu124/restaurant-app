import React from 'react';
import { Card, Typography, Alert, Icon } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import 'antd/dist/antd.css';
import router from 'umi/router';
import FoodTable from './food-table';

class FoodView extends React.Component {

    constructor(props) {
        super(props);

        const token =  localStorage.getItem('restaurant-app-token');
        
        if (!token) {
            router.push(`/user/login`);
        }
    }


    render() {
        const currentUser =  JSON.parse(localStorage.getItem('current-user'));
        return (
            <PageHeaderWrapper content="Foods">
            <Card>
            <FoodTable currentUser={currentUser}/>
            </Card>
          
        </PageHeaderWrapper>
        );
    }
}


export default FoodView;
          
