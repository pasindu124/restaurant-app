import React from 'react';
import { Card, Typography, Alert, Icon } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import 'antd/dist/antd.css';
import router from 'umi/router';
import FoodAddForm from './food-add-form';
import clickApi from '../../../src/services/clickapi';


class FoodView extends React.Component {
    
    constructor(props) {
        super(props);
        this.clickApi = new clickApi();
        this.currentFood = {}

        const token =  localStorage.getItem('restaurant-app-token');
        this.handleAddFood = this.handleAddFood.bind(this);
        if (!token) {
            router.push(`/user/login`);
        }
    }

    handleAddFood(values) {
        const currentUser =  JSON.parse(localStorage.getItem('current-user'));
        values.username = currentUser.username;

        this.clickApi.postData(values, 'fooditem' ).then((res) => {
            router.push(`/foods/list`);
        });
    }

    render() {
        
        return (
            <PageHeaderWrapper content="Foods Add">
            <Card>
                <FoodAddForm callBackSubmit={this.handleAddFood} currentFood={this.currentFood}/>
            </Card>
          
        </PageHeaderWrapper>
        );
    }
}


export default FoodView;
          
