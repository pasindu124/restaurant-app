import React from 'react';
import { Card, Typography, Alert, Icon } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import 'antd/dist/antd.css';
import router from 'umi/router';

class HomeView extends React.Component {

    constructor(props) {
        super(props);

        const token =  localStorage.getItem('restaurant-app-token');
        if (!token) {
            router.push(`/user/login`);
        }
    }

    render() {
        return (
            <PageHeaderWrapper content="Home">
            <Card>
            
            </Card>
            
        </PageHeaderWrapper>
        );
    }
}


export default HomeView;
          
