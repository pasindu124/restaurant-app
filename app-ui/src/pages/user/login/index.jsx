
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import styles from './style.less';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import Link from 'umi/link';
import clickApi from '../../../services/clickapi';
import router from 'umi/router';

class NormalLoginForm extends React.Component {
  state = {
    loginError: false
  }
  constructor(props) {
    super(props);
    this.clickApi = new clickApi();
    const token =  localStorage.getItem('restaurant-app-token');
    if (token) {
      router.push(`/home`);
    }
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        return this.clickApi.userRegister(values, 'authenticate').then(response => {
          if (response.token) {
            localStorage.setItem('restaurant-app-token', response.token);
            return this.clickApi.getCurrentUser(response, 'currentUser').then(response => {
              localStorage.setItem('current-user', JSON.stringify(response));
              router.push(`/home`);
            })
          } else {
            this.setState({loginError: true})
            router.push(`/user/login`);
          }
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className={styles.loginform}>
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username"
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />,
          )}
        </Form.Item>
        {this.state.loginError && 
        <p className={styles.errorMessage}>Invalid Username/Password </p>
        }
        <Form.Item>
          <Button type="primary" htmlType="submit" className={styles.loginformbutton}>
            Log in
          </Button>
          Or <Link to="/user/register">register now!</Link>
        </Form.Item>
      </Form>
    );
  }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(NormalLoginForm);

export default WrappedNormalLoginForm;
          