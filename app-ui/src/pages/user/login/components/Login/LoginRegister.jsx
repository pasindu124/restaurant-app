import { Button, Form } from 'antd';
import React from 'react';
import classNames from 'classnames';
import styles from './index.less';
const FormItem = Form.Item;

const LoginRegister = ({ className, ...rest }) => {
  const clsString = classNames(styles.submit, className);
  return (
    <FormItem>
      <Button size="large" className={clsString} type="danger" {...rest} />
    </FormItem>
  );
};

export default LoginRegister;
