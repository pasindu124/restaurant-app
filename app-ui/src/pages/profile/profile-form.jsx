
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import {
  Form,
  Input,
  Tooltip,
  Icon,
  Cascader,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  AutoComplete,
} from 'antd';


class RegistrationForm extends React.Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.callbackUpdateUser(values);
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  handleWebsiteChange = value => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
    }
    this.setState({ autoCompleteResult });
  };

  render() {
    const { getFieldDecorator } = this.props.form;  

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    

    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item
          label="Username"
        >
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!', whitespace: true ,readonly: true}],
            initialValue: this.props.curentUser.username
          })(<Input readOnly/>)}
        </Form.Item>
        <Form.Item
          label="Name"
        >
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please input your restaurant name!', whitespace: true }],
            initialValue: this.props.curentUser.name
          })(<Input />)}
        </Form.Item>
        <Form.Item
          label="TelNo"
        >
          {getFieldDecorator('telno', {
            rules: [{ required: true, message: 'Please input your restaurant name!', whitespace: true }],
            initialValue: this.props.curentUser.telno
          })(<Input />)}
        </Form.Item>
        <Form.Item
          label="Address"
        >
          {getFieldDecorator('address', {
            rules: [{ required: true, message: 'Please input your restaurant name!', whitespace: true }],
            initialValue: this.props.curentUser.address
          })(<Input />)}
        </Form.Item>
        <Form.Item
          label="City"
        >
          {getFieldDecorator('city', {
            rules: [{ required: true, message: 'Please input your restaurant name!', whitespace: true }],
            initialValue: this.props.curentUser.city
          })(<Input />)}
        </Form.Item>
        
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Update
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const ProfileForm = Form.create({ name: 'update' })(RegistrationForm);
export default ProfileForm;
          