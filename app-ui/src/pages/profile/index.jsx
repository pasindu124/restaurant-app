import React from 'react';
import { Card, Typography, Alert, Icon } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import 'antd/dist/antd.css';
import router from 'umi/router';
import ProfileForm from './profile-form';
import clickApi from '../../../src/services/clickapi';


class ProfileView extends React.Component {
   
    
    constructor(props) {
        super(props);
        this.clickApi = new clickApi();
        
        const token =  localStorage.getItem('restaurant-app-token');
        if (!token) {
            router.push(`/user/login`);
        }

        
        
    }
    updateUser = (values) => {
        return this.clickApi.putData(values, 'updateUser').then(response => {
          if (response.username) {
            localStorage.setItem('current-user', JSON.stringify(response))
            router.push(`/home`);
          }
        });
    };

    render() {
        let curentUser = JSON.parse(localStorage.getItem('current-user'));;
        return (
            <PageHeaderWrapper content="Profile">
            <Card>
                <ProfileForm curentUser={curentUser} callbackUpdateUser={this.updateUser}/>
            </Card>
           
            </PageHeaderWrapper>
        );
    }
}


export default ProfileView;
          
