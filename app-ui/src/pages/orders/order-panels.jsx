
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { Collapse, Button, Alert, Spin ,Col, Row, Card, Tag} from 'antd';
import styles from './index.less'
import clickApi from '../../../src/services/clickapi';

const { Panel    } = Collapse;
const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;
class PanelView extends React.Component {
    state = {
        message: 'pasindu',
        data: {}
    }
    constructor(props) {
        super(props);

        console.log(this.props);
        this.clickApi = new clickApi();

        this.state = {
            dataObj:props.orders
        };
        console.log(this.state.dataObject)
    }

    componentDidMount () {
         this.fetchData();
        
    }
    
    fetchData() {
        const currentUser =  JSON.parse(localStorage.getItem('current-user'));
        return this.clickApi.getData(`orders/?username=${currentUser.username}`).then((res) => {
         this.setState({data : res});
        });
    }

    acceptOrder(id){
        return this.clickApi.postData({id: id},`orders/accept`).then((res) => {
            this.fetchData();
        });
    }

    rejectOrder(id){
        return this.clickApi.postData({id: id},`orders/reject`).then((res) => {
            this.fetchData();
        });
    }

    requestRider(id){
        return this.clickApi.postData({id: id},`kafka-server/requestrider`).then((res) => {
            this.fetchData();
        });
    }

    render() {
    
        return (
            <Collapse accordion>
                { this.state && this.state.data &&
                    this.state.data.map((item)=>(
                        <Panel header={[`Order ${item.id} `, <Tag color="blue">{item.order_status}</Tag>,  <Tag color="green">{item.delivery_status}</Tag>]} key={item.id}>
                
                            <li> <Alert message={`Order status: ${item.order_status}`} type="info" /></li>
                            <li> <Alert message={`Delivery status: ${item.delivery_status}`} type="info" /></li> <br/>
                            

                            <div style={{ background: '#ECECEC', padding: '30px' }}>
                                <Row gutter={16}>
                                <Col span={8}>
                                    <Card title="Customer details" bordered={false}>
                                        
                                            <li>
                                            Name: <b>{item.cusname}</b>
                                            </li>
                                            <li>
                                            Telno: <b>{item.custel}</b>
                                            </li>
                                        
                                     
                                    </Card>
                                </Col>
                                <Col span={8}>
                                    <Card title="Rider details" bordered={false}>

                                    {item.delivery_status == 'requested' && 
                            
                                    <div>
                                        <Spin />  <b>Waiting for rider</b>
                                    </div>
                                    
                                    }
                                    {(item.delivery_status == 'ongoing' || item.delivery_status == 'delivered') &&
                                        <div>
                                           
                                        <li>
                                        Name: <b>{item.ridername}</b>
                                        </li>
                                        <li>
                                        Telno: <b>{item.ridertel}</b>
                                        </li>
                                        </div>
                                        
                                    }
                                    </Card>
                                </Col>
                                <Col span={8}>
                                    <Card title="Order details" bordered={false}>
                                            <li>
                                            Chiken rice - Rs.230
                                            </li>
                                            <li>
                                            Mango juice - Rs.130
                                            </li>
                                    </Card>
                                </Col>
                                </Row>
                            </div>
                            
                        <Button type="primary" className={styles.mr10} onClick={(e) => this.acceptOrder(item.id)} disabled={item.order_status =='accepted' || item.order_status =='rejected'}>Accept</Button>
                        <Button type="danger" disabled={item.order_status =='accepted' || item.order_status =='rejected' } onClick={(e) => this.rejectOrder(item.id)}>Reject</Button>
                        <Button type="primary" disabled={(item.order_status =='pending' || item.order_status =='rejected' || item.delivery_status =='requested' || item.delivery_status =='ongoing' || item.delivery_status =='delivered')} className={styles.mr10} onClick={(e) => this.requestRider(item.id)}>Request Rider</Button>
                        </Panel> 
                    ))
                }
            </Collapse>
    
        );
    }
}


export default PanelView;       