import React from 'react';
import { Card, Typography, Alert, Icon } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import 'antd/dist/antd.css';
import router from 'umi/router';
import PanelView from './order-panels.jsx';
import clickApi from '../../../src/services/clickapi';
class OrderView extends React.Component {
    state= {
        orders: {}
    }
    constructor(props) {
        super(props);

        const token =  localStorage.getItem('restaurant-app-token');
        if (!token) {
            router.push(`/user/login`);
        }
        this.clickApi = new clickApi();
        // this.eventSource = new EventSource("http://localhost:8080/notification");
        // this.eventSource.onopen = e => console.log('open');
        // this.eventSource.onmessage = e => {
		// 	const msg = e.data;
		// 	this.setState({message: e.data});
		// };
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        const currentUser =  JSON.parse(localStorage.getItem('current-user'));
        this.clickApi.getData(`orders/?username=${currentUser.username}`).then((res) => {
            this.setState({orders:res})

        });
    }

    render() {

        return (
            <PageHeaderWrapper content="Home">
            <Card>
            <PanelView orders={this.state.orders}/>
            </Card>
            
        </PageHeaderWrapper>
        );
    }
}


export default OrderView;
          
