const baseUrl = 'http://localhost:8080/';
class clickApi {

  getToken() {
    const token = localStorage.getItem('restaurant-app-token');
    return token;
  }


  async userRegister(values, url) {
    const headers = new Headers();
    headers.append('Content-type', 'application/json');

    const options = {
      method: 'POST',
      headers,
      body: JSON.stringify(values),
    };
    const request = new Request(baseUrl + url, options);

    return fetch(request).then(response => response.json());
  }

  async authenticate(values, url) {
    const headers = new Headers();
    headers.append('Content-type', 'application/json');

    const options = {
      method: 'POST',
      headers,
      body: JSON.stringify(values),
    };
    const request = new Request(baseUrl + url, options);

    return fetch(request).then(response => response.json());
  }

  async getCurrentUser(values, url) {
    const headers = new Headers();
    headers.append('Content-type', 'application/json');

    const options = {
      method: 'POST',
      headers,
      body: JSON.stringify(values),
    };
    const request = new Request(baseUrl + url, options);

    return fetch(request).then(response => response.json());
  }

  async putData(values, url) {
    const options = {
      method: 'PUT',
      headers: {
        'Authorization': 'Bearer ' + this.getToken(),
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(values),
    };
    const request = new Request(baseUrl + url, options);

    return fetch(request).then(response => response.json());
  }

  async postData(values, url) {
    const options = {
      method: 'POST',
      headers: {
        'Authorization': 'Bearer ' + this.getToken(),
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(values),
    };
    const request = new Request(baseUrl + url, options);

    return fetch(request).then(response => response.json());
  }

  async getData( url) {
    const options = {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + this.getToken(),
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    };
    const request = new Request(baseUrl + url, options);

    return fetch(request).then(response => response.json())
  }

  async deleteData(url) {
    const options = {
      method: 'DELETE',
      headers: {
        'Authorization': 'Bearer ' + this.getToken(),
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    };
    const request = new Request(baseUrl + url, options);

    return fetch(request).then(response => response.json());
  }
}
export default clickApi;
